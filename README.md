# 🎊 Quick Start

Pré-requis sur le poste : 
- [Anisble](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [Vagrant](https://developer.hashicorp.com/vagrant/downloads)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

🗒️ Instruction :

1. Cloner le dépot : `git clone git@forgemia.inra.fr:aurelien.djian1/sandbox-ansible.git`
2. Se déplacer dans le dossier : `cd sandbox-ansible`
3. Démarrer la VM : `vagrant up`
4. It's done ! 👍

Il ne reste plus qu'à s'amuser ! 🥳

Ansible étant préconfiguré, on peut exécuter directement les commandes suivantes (par exemple) : 
- `ansible -m ping sandbox`
- `ansible-playbook my_beautiful_playbook.yml`

# 📖 Introduction

L'objectif de ce dépôt est de créer une machine virtuelle (provider Virtualbox) en utilisant Vagrant. Cette machine virtuelle nous servira ensuite à jouer des playbooks Ansible ou autres.

**Le dépôt est prêt à être utilisé !**

Arborescence du dossier :

```
aurelien@devops ~/D/test> tree 
.
├── ansible.cfg
├── data
├── inventory
│   └── vagrant.yml
└── Vagrantfile

2 directories, 3 files
```

# Vagrant

## Installation

Sur une machine de type Debian/Ubuntu :
```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install vagrant
```

## Les commandes de base Vagrant

- Status de vagrant : `vagrant status`
- Démmarer la configuration : `vagrant up`
- Se connecter en SSH à la machine : `vagrant ssh`
- Arrêter la machine : `vagrant halt`
- ⚠️ Supprimer la machine virtuelle (destruction complète) : `vagrant destroy`

## Configuration

### Vagrantfile

Création d'une machine virtuelle avec les caractéristiques suivantes :
 - Nom : sandbox
 - Mémoire : 1024 Mo
 - Forward des ports :
   - local : distant
   - 22 : 2022
   - 80 : 8080
   - 443 : 443

Deux options de configuration :
- HOSTNAME : pour changer le nom de la VM
- MEMORY : pour changer la taille de la RAM de la VM

[Vagrantfile](Vagrantfile)

### Démarrage de la machine

Démarrer la machine virtuelle avec Vagrant.

```bash
vagrant up
```

### SSH

Pour se connecter en ssh à la machine nouvellement créé : `vagrant ssh`

Pour afficher l'ensemble des informations concernant la clé ssh : `vagrant ssh-config`
```bash
Host default
  HostName 127.0.0.1
  User vagrant
  Port 2202
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /home/aureld/Downloads/test/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

Pour se connecter depuis le terminal : 
```bash
ssh vagrant@127.0.0.1 -p 2202 -i /home/aureld/Downloads/test/.vagrant/machines/default/virtualbox/private_key
```

# Ansible

## Configuration

### Inventaire

Création de la machine sandbox01 avec le port 2202 et la clé privée Vagrant.

```yaml
---
sandbox:
  hosts:
    sandbox01:
      ansible_port: 2202
  vars:
    ansible_host: 127.0.0.1
    ansible_user: vagrant
    # the path is relative to the ansible.cfg configuration file
    ansible_ssh_private_key_file: .vagrant/machines/default/virtualbox/private_key
```

### Configuration

Création d'un fichier de configuration pour simplifier l'exécution des commandes Ansible.

Utilisation de l'option `host_key_checking=False` pour éviter la vérification des clés SSH, pratique lors des déstructions/reconstructions des machines virtuelles avec Vagrant.

```yaml
[defaults]
inventory=inventory/vagrant.yml
host_key_checking=False
stdout_callback=yaml
callback_enabled=timer
```

## Vérification du fonctionnement

```yaml
aureld@devops ~/D/test> ansible sandbox -m ping
sandbox01 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

# 🖐️ Astuces

Pour générer une clé SSH : `ssh-keygen -t rsa -b 4096`

Tester la connexion avec l'utilisateur `ansible` sur la VM : `ssh ansible@localhost -p 2202 -i data/ssh-key-ansible/id_rsa `
